<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

// Including database connections
require_once 'db_con.php';

if(isset($_POST["submit_file"]))
{
 $file = $_FILES["file"]["tmp_name"];

 $file_open = fopen($file,"r");
 while(($csv = fgetcsv($file_open, 1000, ",")) !== false)
 {
  $address = $csv[0];
  $city = $csv[1];
  $sales = $csv[2];

  $stmt = $DBcon->prepare("INSERT INTO sales(address,city,sales) VALUES(:address,:city,:sales)");

  $stmt->bindparam(':address', $address);
  $stmt->bindparam(':city', $city);
  $stmt->bindparam(':sales', $sales);
  $stmt->execute();
 }
}

echo "Imported Successfully";
?>
